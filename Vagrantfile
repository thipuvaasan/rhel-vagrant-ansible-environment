# -*- mode: ruby -*-
# vi: set ft=ruby :

####################################################################################################
# Vagrantfile RHEL Developer Machines for Ansible Provisioning
#
# This Vagrantfile provisions 1 to 5 virtual machines with Red Hat Enterprise Linux and all
# requirements needed to further provision the machines with Ansible. 
# It contains definition for the following VMs:
#  * 5 RHEL7 VMs as General Nodes (vm1, vm2, vm3, vm4, vm5)
#
# Use "vagrant up vmX" (with X = 1..5) to start a single machine. Use "vagrant up" to start all
# machines.
#
# As these machines are meant to be used for local testing purposes, the firewalls are initially 
# turned off. If you want to test firewall related provisionings, please remove the disablefirewall
# provisioner from the code below.
#
# To use these virtual machines, you need a developer license for the RHEL operating system. A
# license for 16 machines installations is included when you create a free Red Hat Developer Account
# at https://developers.redhat.com. 
# Please provide the credentials to your Red Hat Developer Account by defining the environment 
# variables REDHAT_ACCOUNT_USER and REDHAT_ACCOUNT_PASS prior to running this Vagrantfile. 
# The Vagrantfile will then register the created VMs automatically in your Red Hat Developer Account
# at initial provisioning and then also unregister them at machine destruction so that your license
# is freed up again after use.
####################################################################################################

# Read and check necessary environment variables
$user = ENV['REDHAT_ACCOUNT_USER']
$pass = ENV['REDHAT_ACCOUNT_PASS']

if $user.nil? || $pass.nil?
  puts "Missing Red Hat Subscription credentials. Please provide Red Hat Subscription credentials 
        in environment variables REDHAT_ACCOUNT_USER and REDHAT_ACCOUNT_PASS and rerun the command. 
        See Vagrantfile for more details about this."
  exit 1
end

# Provisioner: Register the VM for the Redhat Developer Subscription
$registervm = %{
if ! subscription-manager status; then
  sudo subscription-manager register --username=#{$user} --password=#{$pass} --auto-attach
  sudo yum -y update
fi
}

# Provisioner: Install Python as a prerequisite for Ansible
$installpython = %{
if ! python --version; then 
  sudo yum -y install python2
fi
}

# Provisioner: Disable Firewalls
# Attention: Don't do this in production :) 
$disablefirewall = %{
  systemctl disable firewalld
  systemctl stop firewalld
}

Vagrant.configure("2") do |config|
  config.vbguest.auto_update = false

  config.ssh.private_key_path = "_insecure_private_key"
  config.ssh.insert_key = false

  config.trigger.before :destroy do |t|
    t.info = "Unregistering the machine from Redhat Developer Subscription"
    t.run_remote = {inline: "sudo subscription-manager unregister"}
  end

  config.vm.box = "generic/rhel7"
  config.vm.provision "registervm" , type: "shell", inline: $registervm
  config.vm.provision "disablefirewall", type: "shell", inline: $disablefirewall
  config.vm.provision "installpython", type: "shell", inline: $installpython

  (1..5).each do |i|
    config.vm.define "vm#{i}" do |vm|
      vm.vm.hostname = "vm#{i}"
      vm.vm.network "private_network", ip: "10.0.0.21#{i}"

      vm.vm.provider :virtualbox do |vbox|
        vbox.name = "vm#{i}"
        vbox.cpus = 1
        vbox.memory = 512
      end
    end
  end
end
