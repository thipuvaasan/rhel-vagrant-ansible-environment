Vagrant Provisioning of RHEL Virtual Machines for Ansible Provisioning
======================================================================

This repository contains a complete Vagrant setup to provision virtual machines with Red Hat Enterprise Linux installed and all prerequisites for further provisioning of these machines with Ansible provided.

You can use this repository as the basis for your own infrastructure automation projects... or just as a way to intelligently provision properly licensed RHEL virtual machines. 

The virtual machines are configured for local development use only: The firewalls of the machines are turned off and they use an insecure (publicly shared in this repository) SSL key for access. This is done to make using this code as easy as possible and to include everything you need in the repository. Obviously, don't expose your virtual machines outside of your local environment configured like this.

Getting Started
---------------

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
Install Vagrant on your machine by following the instructions at https://www.vagrantup.com/docs/installation/

To use RHEL as the operating system for your virtual machines, you need a license for the RHEL operating system. A developer license for 16 machines installations is included when you create a free Red Hat Developer Account at https://developers.redhat.com. 

Please provide the credentials to your Red Hat Developer Account by defining the environment variables REDHAT_ACCOUNT_USER and REDHAT_ACCOUNT_PASS prior to running this Vagrantfile.  The Vagrantfile will then register the created VMs automatically in your Red Hat Developer Account at initial provisioning and then also unregister them at machine destruction so that your license
is freed up again after use:

```sh
export REDHAT_ACCOUNT_USER=<...>
export REDHAT_ACCOUNT_PASS=<...>
```

### Installation

Once you have provided all the prerequisites (see above), no further installation is required.

### Usage

To provision all machines defined in the Vagrant file, head to the repository directory and run vagrant up as:

```sh
export REDHAT_ACCOUNT_USER=<...>
export REDHAT_ACCOUNT_PASS=<...>
vagrant up
```

To provision a single machine defined in the Vagrant file, head to the repository directory and run vagrant up as:

```sh
export REDHAT_ACCOUNT_USER=<...>
export REDHAT_ACCOUNT_PASS=<...>
vagrant up vm<1..5>
```

To destroy all machines defined in the Vagrant file, head to the repository directory and run vagrant destroy as:

```sh
export REDHAT_ACCOUNT_USER=<...>
export REDHAT_ACCOUNT_PASS=<...>
vagrant destroy
```

To destroy a single machine defined in the Vagrant file, head to the repository directory and run vagrant destroy as:

```sh
export REDHAT_ACCOUNT_USER=<...>
export REDHAT_ACCOUNT_PASS=<...>
vagrant destroy vm<1..5>
```

Built With
----------

* [Vagrant](https://www.vagrantup.com) - vagrant provisioner
* [Ruby](https://www.ruby-lang.org) - ruby programming language

Contributing
------------

Please read [CONTRIBUTING.md](https://github.com/patrick-vonsteht/rhel-vagrant-ansible-environment/blob/master/CONTRIBUTING.md) for details on the code of conduct, and the process for submitting pull requests.

Versioning
----------

We use SemVer for versioning. For the versions available, see the [tags on this repository](https://github.com/patrick-vonsteht/rhel-vagrant-ansible-environment/tags).

Authors
-------

* [Patrick von Steht](https://github.com/patrick-vonsteht) - Initial Work

License
-------

This project is licensed under the MIT License - see the [LICENSE.md](https://github.com/patrick-vonsteht/rhel-vagrant-ansible-environment/blob/master/LICENSE.md) file for details.

Acknowledgments
---------------

* This README is based on the [Billie Thompsons README template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
