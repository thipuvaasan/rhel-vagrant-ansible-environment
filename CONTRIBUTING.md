# Contributing to this repository

First off, thank you for considering contributing to this repository! It's great that you gift your time to do this!

There's no predefined process for contributing as I don't expect complex contributions on this small repository.

## Do you want to discuss a possible change or extension before you dive into work? 
Feel free to create a Github issue to discuss. I'll get back to you as soon as possible.

## Do you already have a change or extension ready that you want to contribute? 
Feel free to create a Github pull request. I'll get back to you as soon as possible.
